#!/bin/bash

FASTAPI_URL="$FASTAPI_URL"

# Récupérez les arguments
USERNAME="$USERNAME"
PASSWORD="$PASSWORD"
SENTENCE="$SENTENCE"

URL="${FASTAPI_URL}?username=${USERNAME}&password=${PASSWORD}&sentence=${SENTENCE}"
curl "$URL"

# Utilisation des données concernant alice

# Informations d'authentification
USERNAME="alice"
PASSWORD="wonderland"
SENTENCE="life is beautiful"

SENTENCE_ENCODED=$(echo "$SENTENCE" | sed 's/ /%20/g')

URL="${FASTAPI_URL}/v1/sentiment?username=${USERNAME}&password=${PASSWORD}&sentence=${SENTENCE_ENCODED}"
echo "url=${URL}"
curl "$URL"

URL="${FASTAPI_URL}/v2/sentiment?username=${USERNAME}&password=${PASSWORD}&sentence=${SENTENCE_ENCODED}"
echo "url=${URL}"
curl "$URL"

SENTENCE="that sucks"

SENTENCE_ENCODED=$(echo "$SENTENCE" | sed 's/ /%20/g')

URL="${FASTAPI_URL}/v1/sentiment?username=${USERNAME}&password=${PASSWORD}&sentence=${SENTENCE_ENCODED}"
echo "url=${URL}"
curl "$URL"

URL="${FASTAPI_URL}/v2/sentiment?username=${USERNAME}&password=${PASSWORD}&sentence=${SENTENCE_ENCODED}"
echo "url=${URL}"
curl "$URL"
