Attaching to evaluation_docker_sentiment_1, evaluation_docker_permission_1, evaluation_docker_fastapi-app_1
[36mfastapi-app_1  |[0m INFO:     Started server process [7]
[36mfastapi-app_1  |[0m INFO:     Waiting for application startup.
[36mfastapi-app_1  |[0m INFO:     Application startup complete.
[36mfastapi-app_1  |[0m INFO:     Uvicorn running on http://0.0.0.0:8000 (Press CTRL+C to quit)
[36mfastapi-app_1  |[0m INFO:     172.25.0.3:45204 - "GET /permissions?username=bob&password=builder HTTP/1.1" 200 OK
[36mfastapi-app_1  |[0m INFO:     172.25.0.3:45214 - "GET /permissions?username=alice&password=wonderland HTTP/1.1" 200 OK
[36mfastapi-app_1  |[0m INFO:     172.25.0.4:54888 - "GET /?username=bob&password=builder&sentence=hello HTTP/1.1" 404 Not Found
[36mfastapi-app_1  |[0m INFO:     172.25.0.3:45224 - "GET /permissions?username=bob&password=builder HTTP/1.1" 200 OK
[36mfastapi-app_1  |[0m INFO:     172.25.0.4:54900 - "GET /v1/sentiment?username=alice&password=wonderland&sentence=life%20is%20beautiful HTTP/1.1" 200 OK
[36mfastapi-app_1  |[0m INFO:     172.25.0.3:45228 - "GET /permissions?username=clementine&password=mandarine HTTP/1.1" 403 Forbidden
[36mfastapi-app_1  |[0m INFO:     172.25.0.4:54908 - "GET /v2/sentiment?username=alice&password=wonderland&sentence=life%20is%20beautiful HTTP/1.1" 200 OK
[36mfastapi-app_1  |[0m INFO:     172.25.0.4:54916 - "GET /v1/sentiment?username=alice&password=wonderland&sentence=that%20sucks HTTP/1.1" 200 OK
[36mfastapi-app_1  |[0m INFO:     172.25.0.4:54930 - "GET /v2/sentiment?username=alice&password=wonderland&sentence=that%20sucks HTTP/1.1" 200 OK
[36mfastapi-app_1  |[0m INFO:     172.25.0.3:60594 - "GET /?username=bob&password=builder&sentence=hello HTTP/1.1" 404 Not Found
[36mfastapi-app_1  |[0m INFO:     172.25.0.3:60596 - "GET /v1/sentiment?username=alice&password=wonderland&sentence=life%20is%20beautiful HTTP/1.1" 200 OK
[36mfastapi-app_1  |[0m INFO:     172.25.0.3:60612 - "GET /v2/sentiment?username=alice&password=wonderland&sentence=life%20is%20beautiful HTTP/1.1" 200 OK
[36mfastapi-app_1  |[0m INFO:     172.25.0.3:60628 - "GET /v1/sentiment?username=alice&password=wonderland&sentence=that%20sucks HTTP/1.1" 200 OK
[36mfastapi-app_1  |[0m INFO:     172.25.0.3:60640 - "GET /v2/sentiment?username=alice&password=wonderland&sentence=that%20sucks HTTP/1.1" 200 OK
[36mfastapi-app_1  |[0m INFO:     172.25.0.4:52344 - "GET /permissions?username=bob&password=builder HTTP/1.1" 200 OK
[36mfastapi-app_1  |[0m INFO:     172.25.0.4:52352 - "GET /permissions?username=alice&password=wonderland HTTP/1.1" 200 OK
[36mfastapi-app_1  |[0m INFO:     172.25.0.4:52356 - "GET /permissions?username=bob&password=builder HTTP/1.1" 200 OK
[36mfastapi-app_1  |[0m INFO:     172.25.0.4:52360 - "GET /permissions?username=clementine&password=mandarine HTTP/1.1" 403 Forbidden
[33mpermission_1   |[0m   % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
[33mpermission_1   |[0m                                  Dload  Upload   Total   Spent    Left  Speed
[33mpermission_1   |[0m   0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0100    39  100    39    0     0   8487      0 --:--:-- --:--:-- --:--:--  9750
[33mpermission_1   |[0m {"username":"bob","permissions":["v1"]}test des identifiants donnés dans l'évaluation
[33mpermission_1   |[0m Appel de l'URL avec username: alice et password: wonderland
[33mpermission_1   |[0m   % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
[33mpermission_1   |[0m                                  Dload  Upload   Total   Spent    Left  Speed
[33mpermission_1   |[0m   0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0100    46  100    46    0     0  11105      0 --:--:-- --:--:-- --:--:-- 11500
[33mpermission_1   |[0m {"username":"alice","permissions":["v1","v2"]}Appel de l'URL avec username: bob et password: builder
[33mpermission_1   |[0m   % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
[33mpermission_1   |[0m                                  Dload  Upload   Total   Spent    Left  Speed
[33mpermission_1   |[0m   0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0100    39  100    39    0     0   1554      0 --:--:-- --:--:-- --:--:--  1625
[33mpermission_1   |[0m {"username":"bob","permissions":["v1"]}Appel de l'URL avec username: clementine et password: mandarine
[33mpermission_1   |[0m   % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
[33mpermission_1   |[0m                                  Dload  Upload   Total   Spent    Left  Speed
[33mpermission_1   |[0m   0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0100    34  100    34    0     0   1643      0 --:--:-- --:--:-- --:--:--  1700
[33mpermission_1   |[0m {"detail":"Authentication failed"}  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
[33mpermission_1   |[0m                                  Dload  Upload   Total   Spent    Left  Speed
[33mpermission_1   |[0m   0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0100    39  100    39    0     0   7239      0 --:--:-- --:--:-- --:--:--  7800
[33mpermission_1   |[0m {"username":"bob","permissions":["v1"]}test des identifiants donnés dans l'évaluation
[33mpermission_1   |[0m Appel de l'URL avec username: alice et password: wonderland
[33mpermission_1   |[0m   % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
[33mpermission_1   |[0m                                  Dload  Upload   Total   Spent    Left  Speed
[33mpermission_1   |[0m   0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0100    46  100    46    0     0   7712      0 --:--:-- --:--:-- --:--:--  9200
[33mpermission_1   |[0m {"username":"alice","permissions":["v1","v2"]}Appel de l'URL avec username: bob et password: builder
[33mpermission_1   |[0m   % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
[33mpermission_1   |[0m                                  Dload  Upload   Total   Spent    Left  Speed
[33mpermission_1   |[0m   0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0100    39  100    39    0     0   4738      0 --:--:-- --:--:-- --:--:--  4875
[33mpermission_1   |[0m {"username":"bob","permissions":["v1"]}Appel de l'URL avec username: clementine et password: mandarine
[33mpermission_1   |[0m   % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
[33mpermission_1   |[0m                                  Dload  Upload   Total   Spent    Left  Speed
[33mpermission_1   |[0m   0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0100    34  100    34    0     0   3786      0 --:--:-- --:--:-- --:--:--  4250
[33mpermission_1   |[0m {"detail":"Authentication failed"}[32msentiment_1    |[0m   % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
[32msentiment_1    |[0m                                  Dload  Upload   Total   Spent    Left  Speed
[32msentiment_1    |[0m   0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0100    22  100    22    0     0   1811      0 --:--:-- --:--:-- --:--:--  2000
[32msentiment_1    |[0m {"detail":"Not Found"}url=http://fastapi-app:8000/v1/sentiment?username=alice&password=wonderland&sentence=life%20is%20beautiful
[32msentiment_1    |[0m   % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
[32msentiment_1    |[0m                                  Dload  Upload   Total   Spent    Left  Speed
[32msentiment_1    |[0m   0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0100    81  100    81    0     0   6016      0 --:--:-- --:--:-- --:--:--  6230
[32msentiment_1    |[0m {"username":"alice","version":"v1","sentence":"life is beautiful","score":0.5994}url=http://fastapi-app:8000/v2/sentiment?username=alice&password=wonderland&sentence=life%20is%20beautiful
[32msentiment_1    |[0m   % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
[32msentiment_1    |[0m                                  Dload  Upload   Total   Spent    Left  Speed
[32msentiment_1    |[0m   0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0100    81  100    81    0     0   6714      0 --:--:-- --:--:-- --:--:--  7363
[32msentiment_1    |[0m {"username":"alice","version":"v2","sentence":"life is beautiful","score":0.5994}url=http://fastapi-app:8000/v1/sentiment?username=alice&password=wonderland&sentence=that%20sucks
[32msentiment_1    |[0m   % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
[32msentiment_1    |[0m                                  Dload  Upload   Total   Spent    Left  Speed
[32msentiment_1    |[0m   0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0100    75  100    75    0     0  20996      0 --:--:-- --:--:-- --:--:-- 25000
[32msentiment_1    |[0m {"username":"alice","version":"v1","sentence":"that sucks","score":-0.3612}url=http://fastapi-app:8000/v2/sentiment?username=alice&password=wonderland&sentence=that%20sucks
[32msentiment_1    |[0m   % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
[32msentiment_1    |[0m                                  Dload  Upload   Total   Spent    Left  Speed
[32msentiment_1    |[0m   0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0100    75  100    75    0     0  23764      0 --:--:-- --:--:-- --:--:-- 25000
[32msentiment_1    |[0m {"username":"alice","version":"v2","sentence":"that sucks","score":-0.3612}  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
[32msentiment_1    |[0m                                  Dload  Upload   Total   Spent    Left  Speed
[32msentiment_1    |[0m   0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0100    22  100    22    0     0   4091      0 --:--:-- --:--:-- --:--:--  4400
[32msentiment_1    |[0m {"detail":"Not Found"}url=http://fastapi-app:8000/v1/sentiment?username=alice&password=wonderland&sentence=life%20is%20beautiful
[32msentiment_1    |[0m   % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
[32msentiment_1    |[0m                                  Dload  Upload   Total   Spent    Left  Speed
[32msentiment_1    |[0m   0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0100    81  100    81    0     0  16510      0 --:--:-- --:--:-- --:--:-- 20250
[32msentiment_1    |[0m {"username":"alice","version":"v1","sentence":"life is beautiful","score":0.5994}url=http://fastapi-app:8000/v2/sentiment?username=alice&password=wonderland&sentence=life%20is%20beautiful
[32msentiment_1    |[0m   % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
[32msentiment_1    |[0m                                  Dload  Upload   Total   Spent    Left  Speed
[32msentiment_1    |[0m   0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0100    81  100    81    0     0  14586      0 --:--:-- --:--:-- --:--:-- 16200
[32msentiment_1    |[0m {"username":"alice","version":"v2","sentence":"life is beautiful","score":0.5994}url=http://fastapi-app:8000/v1/sentiment?username=alice&password=wonderland&sentence=that%20sucks
[32msentiment_1    |[0m   % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
[32msentiment_1    |[0m                                  Dload  Upload   Total   Spent    Left  Speed
[32msentiment_1    |[0m   0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0100    75  100    75    0     0   9431      0 --:--:-- --:--:-- --:--:-- 10714
[32msentiment_1    |[0m {"username":"alice","version":"v1","sentence":"that sucks","score":-0.3612}url=http://fastapi-app:8000/v2/sentiment?username=alice&password=wonderland&sentence=that%20sucks
[32msentiment_1    |[0m   % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
[32msentiment_1    |[0m                                  Dload  Upload   Total   Spent    Left  Speed
[32msentiment_1    |[0m   0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0100    75  100    75    0     0  16692      0 --:--:-- --:--:-- --:--:-- 18750
[32msentiment_1    |[0m {"username":"alice","version":"v2","sentence":"that sucks","score":-0.3612}