#!/bin/bash

#FASTAPI_IP=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' evaluation_docker_fastapi-app_1)


FASTAPI_URL="$FASTAPI_URL"

# Récupérez les arguments
USERNAME="$USERNAME"
PASSWORD="$PASSWORD"

URL="${FASTAPI_URL}?username=${USERNAME}&password=${PASSWORD}"
curl "$URL"

echo "test des identifiants donnés dans l'évaluation"

# Tableaux d'username et de password
usernames=("alice" "bob" "clementine")
passwords=("wonderland" "builder" "mandarine")

# Boucle pour parcourir les combinaisons d'username et de password
for i in "${!usernames[@]}"; do
  username="${usernames[$i]}"
  password="${passwords[$i]}"
  
  # Construire l'URL avec les paramètres username et password
  URL="${FASTAPI_URL}?username=${username}&password=${password}"
  
  # Appeler l'URL avec Curl
  echo "Appel de l'URL avec username: $username et password: $password"
  curl "$URL"
done
